﻿using System;

namespace PasswordGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Generate10Passwords();
        }

        public static void Generate10Passwords()
        {
            string sConsolePassword = string.Empty;
            for (int i = 0; i < 10; i++)
            {
                sConsolePassword += GeneratePasssword(new Random().Next()) + '\n';
            }
            Console.WriteLine(sConsolePassword);

            Console.WriteLine("\n\nNyomj meg egy gombot, hogy generálj még 10-et! (ESCAPE kilép)\n\n");

            ConsoleKeyInfo info = Console.ReadKey();
            if (info.Key != ConsoleKey.Escape)
            {
                Generate10Passwords();
            }
        }

        public static string GeneratePasssword(int? seed = null)
        {
            // random szám generáló
            Random random = seed != null ? new Random(Convert.ToInt32(seed)) : new Random();

            string generatedPswrd = string.Empty;

            int randomLength = random.Next(6, 20);

            for (int i = 0; i < randomLength; i++)
            {
                char asciiRange = '\0';
                int asciiRangeLength = 0;

                int asciiRangeInt = random.Next(0, 3);
                // kis/nagybetű vagy szám
                switch (asciiRangeInt)
                {
                    case 0:
                        asciiRange = 'a';
                        asciiRangeLength = 26;
                        break;
                    case 1:
                        asciiRange = 'A';
                        asciiRangeLength = 26;
                        break;
                    case 2:
                        asciiRange = '0';
                        asciiRangeLength = 10;
                        break;
                    default:
                        break;

                }
                generatedPswrd += (char)(random.Next(asciiRangeLength) + asciiRange);
            }

            return generatedPswrd;
        }
    }
}
